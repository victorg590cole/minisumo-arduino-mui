package app.axelvictor.robotmui;

import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    static final String LOG_ID = "ROBOT_MUI";

    private final int BLUETOOTH_ENABLE_REQ_CODE = 1;

    private Accions accions;
    private BluetoothAdapter bluetoothAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        checkBluetooth();
        accions = new AccionsAxelVictor(getResources());
        // accions = new AccionsAndreaLaura(getResources());

        Button atacaButton = (Button) findViewById(R.id.button_ataca);
        assert atacaButton != null;
        atacaButton.setText(accions.atacaName());
        atacaButton.setEnabled(accions.hasAtaca());

        Button enrereButton = (Button) findViewById(R.id.button_enrere);
        assert enrereButton != null;
        enrereButton.setText(accions.enrereName());
        enrereButton.setEnabled(accions.hasEnrere());

        Button aturaButton = (Button) findViewById(R.id.button_atura);
        assert aturaButton != null;
        aturaButton.setText(accions.aturaName());
        aturaButton.setEnabled(accions.hasAtura());

        Button giraButton = (Button) findViewById(R.id.button_gir);
        assert giraButton != null;
        giraButton.setText(accions.giraName());
        giraButton.setEnabled(accions.hasGira());

        Button girRButton = (Button) findViewById(R.id.button_gir_r);
        assert girRButton != null;
        girRButton.setText(accions.girRName());
        girRButton.setEnabled(accions.hasGirR());

        Button girIButton = (Button) findViewById(R.id.button_gir_i);
        assert girIButton != null;
        girIButton.setText(accions.girIName());
        girIButton.setEnabled(accions.hasGirI());

        Button girIRButton = (Button) findViewById(R.id.button_gir_i_r);
        assert girIRButton != null;
        girIRButton.setText(accions.girIRName());
        girIRButton.setEnabled(accions.hasGirIR());

        Button pujarRampaButton = (Button) findViewById(R.id.button_pujar_rampa);
        assert pujarRampaButton != null;
        pujarRampaButton.setText(accions.pujarRampaName());
        pujarRampaButton.setVisibility(accions.hasPujarRampa() ? View.VISIBLE : View.GONE);

        Button baixarRampaButton = (Button) findViewById(R.id.button_baixar_rampa);
        assert baixarRampaButton != null;
        baixarRampaButton.setText(accions.baixarRampaName());
        baixarRampaButton.setVisibility(accions.hasBaixarRampa() ? View.VISIBLE : View.GONE);

        Button autoModeButton = (Button) findViewById(R.id.button_auto_mode);
        assert autoModeButton != null;
        autoModeButton.setText(accions.autoName());
        autoModeButton.setVisibility(accions.hasAutoMode() ? View.VISIBLE : View.GONE);
    }

    public void onClick(View view) {
        checkBluetooth();
        switch (view.getId()) {
            case R.id.button_ataca:
                accions.ataca();
                break;
            case R.id.button_atura:
                accions.atura();
                break;
            case R.id.button_enrere:
                accions.enrere();
                break;
            case R.id.button_gir:
                accions.gira();
                break;
            case R.id.button_gir_r:
                accions.girR();
                break;
            case R.id.button_gir_i:
                accions.girI();
                break;
            case R.id.button_gir_i_r:
                accions.girIR();
                break;
            case R.id.button_pujar_rampa:
                accions.pujarRampa();
                break;
            case R.id.button_baixar_rampa:
                accions.baixarRampa();
                break;
            case R.id.button_auto_mode:
                accions.autoMode();
                break;
            default:
                Log.wtf(LOG_ID, "Unexpected click: " + view.getId());
                throw new RuntimeException();
        }
    }

    private void checkBluetooth() {
        if (bluetoothAdapter == null) {
            // TODO: No hay Bluetooth
        } else if (!bluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, BLUETOOTH_ENABLE_REQ_CODE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case BLUETOOTH_ENABLE_REQ_CODE:
                if (resultCode == RESULT_OK)
                    return;
                else if (resultCode == RESULT_CANCELED)
                    checkBluetooth();
                else {
                    Log.wtf(LOG_ID, "Unexpected result code: " + resultCode);
                    throw new RuntimeException();
                }
                break;
            default:
                Log.wtf(LOG_ID, "Unexpected result: " + requestCode);
                throw new RuntimeException();
        }
    }
}
