package app.axelvictor.robotmui;

import android.content.res.Resources;
import android.support.annotation.NonNull;

public class AccionsAndreaLaura extends Accions {
    public AccionsAndreaLaura(Resources res) {
        super(new ThisCodes(), res);
    }

    @Override
    @NonNull
    public String atacaName() {
        return "Endavant";
    }

    @Override
    @NonNull
    public String enrereName() {
        return "Endarrere";
    }

    @Override
    @NonNull
    public String aturaName() {
        return "Stop";
    }

    @Override
    @NonNull
    public String giraName() {
        return "Esquerra";
    }

    @Override
    @NonNull
    public String girIName() {
        return "Dreta";
    }

    private static class ThisCodes implements Codes {


        @Override
        public int atacaCode() {
            // Como los códigos son en "char", se convierte a int. Esta conversión podría ser implícita,
            // pero se hace explícita por claridad.
            return (int) '8';
        }

        @Override
        public int enrereCode() {
            return (int) '2';
        }

        @Override
        public int aturaCode() {
            return (int) '5';
        }

        @Override
        public int giraCode() {
            return (int) '4';
        }

        @Override
        public int girRCode() {
            return -1;
        }

        @Override
        public int girICode() {
            return (int) '6';
        }

        @Override
        public int girIRCode() {
            return -1; // -1 para deshabilitar
        }

        @Override
        public int pujarRampaCode() {
            return -1;
        }

        @Override
        public int baixarRampaCode() {
            return -1;
        }

        @Override
        public int autoCode() {
            return -1;
        }
    }
}
