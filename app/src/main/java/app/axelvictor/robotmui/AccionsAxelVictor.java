package app.axelvictor.robotmui;

import android.content.res.Resources;

public class AccionsAxelVictor extends Accions {
    public AccionsAxelVictor(Resources res) {
        super(new ThisCodes(), res);
    }

    private static class ThisCodes implements Codes {

        public int aturaCode() {
            return 1;
        }

        public int atacaCode() {
            return 2;
        }

        public int enrereCode() {
            return 3;
        }

        public int giraCode() {
            return 4;
        }

        public int girRCode() {
            return 5;
        }

        public int girICode() {
            return 6;
        }

        public int girIRCode() {
            return 7;
        }

        public int pujarRampaCode() {
            return 8;
        }

        public int baixarRampaCode() {
            return 9;
        }

        public int autoCode() {
            return 10;
        }
    }
}
