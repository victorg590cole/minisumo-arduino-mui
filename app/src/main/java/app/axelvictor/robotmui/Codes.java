package app.axelvictor.robotmui;

public interface Codes {
    int atacaCode();

    int enrereCode();

    int aturaCode();

    int giraCode();

    int girRCode();

    int girICode();

    int girIRCode();

    int pujarRampaCode();

    int baixarRampaCode();

    int autoCode();
}