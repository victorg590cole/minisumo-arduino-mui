package app.axelvictor.robotmui;

import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.util.Log;

public abstract class Accions {
    protected Codes codes;
    private Resources res;

    protected Accions(Codes codes, Resources res) {
        this.codes = codes;
        this.res = res;
    }

    @NonNull
    public String atacaName() {
        return res.getString(R.string.text_ataca);
    }

    @NonNull
    public String enrereName() {
        return res.getString(R.string.text_enrere);
    }

    @NonNull
    public String aturaName() {
        return res.getString(R.string.text_atura);
    }

    @NonNull
    public String giraName() {
        return res.getString(R.string.text_gira);
    }

    @NonNull
    public String girRName() {
        return res.getString(R.string.text_gir_r);
    }

    @NonNull
    public String girIName() {
        return res.getString(R.string.text_gir_i);
    }

    @NonNull
    public String girIRName() {
        return res.getString(R.string.text_gir_i_r);
    }

    @NonNull
    public String pujarRampaName() {
        return res.getString(R.string.text_pujar_rampa);
    }

    @NonNull
    public String baixarRampaName() {
        return res.getString(R.string.text_pujar_rampa);
    }

    @NonNull
    public String autoName() {
        return res.getString(R.string.text_pujar_rampa);
    }

    public final void ataca() {
        Log.d(MainActivity.LOG_ID, "ATACA " + codes.atacaCode());
    }

    public final void enrere() {
        Log.d(MainActivity.LOG_ID, "ENRERE " + codes.enrereCode());
    }

    public final void atura() {
        Log.d(MainActivity.LOG_ID, "ATURA " + codes.aturaCode());
    }

    public final void gira() {
        Log.d(MainActivity.LOG_ID, "GIRA " + codes.giraCode());
    }

    public final void girR() {
        Log.d(MainActivity.LOG_ID, "GIR R " + codes.girRCode());
    }

    public final void girI() {
        Log.d(MainActivity.LOG_ID, "GIR I " + codes.girICode());
    }

    public final void girIR() {
        Log.d(MainActivity.LOG_ID, "GIR IR " + codes.girIRCode());
    }

    public final void pujarRampa() {
        Log.d(MainActivity.LOG_ID, "PUJAR RAMPA " + codes.pujarRampaCode());
    }

    public final void baixarRampa() {
        Log.d(MainActivity.LOG_ID, "BAIXAR RAMPA " + codes.baixarRampaCode());
    }

    public final void autoMode() {
        Log.d(MainActivity.LOG_ID, "AUTOMÀTIC " + codes.autoCode());
    }

    public final boolean hasAtaca() {
        return codes.atacaCode() != -1;
    }

    public final boolean hasEnrere() {
        return codes.enrereCode() != -1;
    }

    public final boolean hasAtura() {
        return codes.aturaCode() != -1;
    }

    public final boolean hasGira() {
        return codes.giraCode() != -1;
    }

    public final boolean hasGirR() {
        return codes.girRCode() != -1;
    }

    public final boolean hasGirI() {
        return codes.girICode() != -1;
    }

    public final boolean hasGirIR() {
        return codes.girIRCode() != -1;
    }

    public final boolean hasPujarRampa() {
        return codes.pujarRampaCode() != -1;
    }

    public final boolean hasBaixarRampa() {
        return codes.baixarRampaCode() != -1;
    }

    public final boolean hasAutoMode() {
        return codes.autoCode() != -1;
    }
}
